import { Component, OnInit } from '@angular/core';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { Discussion, Message } from '../../../shared/entity/chat';
import { Package, PackageState } from '../../../shared/entity/package';
import { ChatService } from '../../../shared/service/back-office/chat.service';
import { PackageService } from '../../../shared/service/back-office/package.service';
import { ProviderService } from '../../../shared/service/back-office/provider.service';
import { UserService } from '../../../shared/service/user/user.service';
import { Provider } from '../../../shared/entity/provider';
import { Transaction, TransactionState } from '../../../shared/entity/transaction';
import { TransactionService } from '../../../shared/service/back-office/transaction.service';
import { getUiIconFromStatusPackage } from '../../../shared/utils/ui.utils';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  unreadProjetList:{pkg:Package,transaction:Transaction,provider:Provider}[] =[];
  constructor(
    private packageService: PackageService,
    private transactionService:TransactionService,
    private userService:UserService,
    private chatRealTimeService:ChatService,
  ){}


  ngOnInit(): void {
  
    this.chatRealTimeService.getUnReadDiscussion()
    .subscribe((disc:Discussion)=>{
      let pack:Package;
      let transaction:Transaction;
      let provider:Provider;
      this.packageService.findPackageById(disc.idProject.toString())
      .then((value:Package)=>{
        pack=value;
        if(value.state==PackageState.SERVICE_IN_TRANSACTION_STATE)
          return this.userService.getUserById(value.idSelectedProvider)
        else return Promise.resolve(new Provider());
      })
      .then((value:Provider)=>{
        provider=value;
        return this.transactionService.getTransactionById(disc.idTransaction)
      })
      .then((value:Transaction)=>{
        transaction=value;
        this.unreadProjetList.push({
          pkg:pack,
          transaction,
          provider
        });
      });
    });
  }

  getPackageIcon(infos:{pkg:Package,transaction:Transaction,provider:Provider}) {
    return getUiIconFromStatusPackage(infos.pkg,infos.transaction);
  }
}
