import { Component, Input, OnInit, Provider } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ServiceOfProvider } from '../../../../shared/entity/provider';

@Component({
  selector: 'app-how-provider-infos',
  templateUrl: './how-provider-infos.component.html',
  styleUrls: ['./how-provider-infos.component.css']
})
export class HowProviderInfosComponent implements OnInit {
  @Input() provider:Provider;
  @Input() service:ServiceOfProvider;
  userProfileImg = '../../../assets/img/user_image.jpg';
  constructor(private bsModalRef:BsModalRef) { }

  ngOnInit(): void {
  }

  cancelModel()
  {
    this.bsModalRef.hide();
  }

}
