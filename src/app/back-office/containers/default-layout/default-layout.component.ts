import { AfterContentInit, AfterViewInit, Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../../shared/service/user/user.service';
import { AuthService } from '../../../shared/service/auth/auth.service';
import { navItems } from '../../../_nav';
import { Discussion, Message } from '../../../shared/entity/chat';
import { ChatService } from '../../../shared/service/back-office/chat.service';
import { Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PackageService } from '../../../shared/service/back-office/package.service';
import { Provider } from '../../../shared/entity/provider';
import { NotificationService } from '../../../shared/service/notification/notification.service';
import { ChatRealtimeService } from '../../../shared/service/back-office/chat-realtime.service';
import { Package, PackageState } from '../../../shared/entity/package';
import { DetailComponent } from '../../../shared/components/modals/detail.component';
// import { TransactionRealtimeService } from '../../../shared/service/back-office/transaction-realtime.service';
import { TransactionService } from '../../../shared/service/back-office/transaction.service';
import { AppHeaderComponent } from '@coreui/angular';
import { Transaction } from '../../../shared/entity/transaction';
import { getUiIconFromStatusPackage } from '../../../shared/utils/ui.utils';
// import { NotificationService } from '../../../shared/service/back-office/notification.service';

declare var $: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.scss']
})
export class DefaultLayoutComponent implements OnInit,AfterViewInit {

  @ViewChild("modalDetail") modalDetail:DetailComponent;

  @ViewChild("appHeader") header:AppHeaderComponent
  public sidebarMinimized = false;
  public navItems = navItems;
  unreadMessageList: Message[] = [];
  unreadProjetList:{pkg:Package,transaction:Transaction,discuss:Discussion}[] =[];
  waitingPackageInformation = true;
  selectedPackage = null;
  errorFindingPackageMessage = '';
  // tslint:disable-next-line:max-line-length
  public userName: String = '';
  closeResult = '';


  // To have a current year for copirygth
  year: Date = new Date();

  today: number = Date.now();

  constructor (private userService: UserService,
    private authService: AuthService,
    private packageService: PackageService,
    private chatService: ChatService,
    private modalService: NgbModal,
    private router:Router,
    private chatRealTimeService:ChatService,
    private transactionService:TransactionService,
    private dashbaord:ElementRef,
    private notification: NotificationService) {
  }
  ngAfterViewInit(): void {
    let parent = this.dashbaord.nativeElement.querySelectorAll(".navbar-toggler")[2];
    // console.log(parent.childNodes)
    let notifButon =this.dashbaord.nativeElement.querySelector("#notifButton");
    // parent.childNodes.forEach(element => {
    //   parent.removeChild(element);
    // });
    parent.removeChild(parent.querySelector(".navbar-toggler-icon"))
    parent.appendChild(notifButon)
    // console.log("notif",notifButon)
  }

  ngOnInit(): void {
    this.authService.currentUserSubject.subscribe((user: Provider) => {
      this.userName = user.getSimpleName();
    });

    this.chatRealTimeService.getUnReadDiscussion()
    .subscribe((discuss:Discussion)=>{
      let pack:Package;
      let transaction:Transaction;
      let provider:Provider;
      this.packageService.findPackageById(discuss.idProject.toString())
      .then((value:Package)=>{
        pack=value;
        if(value.state==PackageState.SERVICE_IN_TRANSACTION_STATE)
          return this.userService.getUserById(value.idSelectedProvider)
        else return Promise.resolve(new Provider());
      })
      .then((value:Provider)=>{
        provider=value;
        return this.transactionService.getTransactionById(discuss.idTransaction)           
      })
      .then((value:Transaction)=>{
        transaction=value;
        this.unreadProjetList.push({
          pkg:pack,
          transaction,
          discuss
        })
      });      
    })

    // this.chatService.listMessageUnreadSubject.subscribe((listMessage) => this.unreadMessageList = listMessage);
  
    // this.chatRealTimeService.getUnReadDiscussion()
    // .subscribe((disc:Discussion)=>{
    //   this.packageService.findPackageById(disc.idProject.toString())
    //   .then((value:Package)=>this.unreadProjetList.push({
    //     pkg:value,
    //     discuss:disc
    //   }))
    // })
    
    
  }
  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }
  logOut() {
      this.authService.logOut();
      // this.notification.showNotification('top', 'right', 'success', '', '\<b>You\'re logged out !\</b>');
  }

  goToChat(idProjet:string)
  {

    // this.router.navigate([`/package_infos`,idProjet]);
  }

  open(content, message: Message) {
    // console.log("Open modal")
    // this.chatService.markAsRead(message._id,message.idDiscussion)
    // .then((result)=>{
    this.packageService.findPackageById(this.chatService.getLocalDiscutionById(message.idDiscussion).idProject)
      .then((r) => {
        this.waitingPackageInformation = false;
        this.selectedPackage = r;
      }).catch((r) => {
        this.waitingPackageInformation = false;
        this.errorFindingPackageMessage = 'Cannot retreived informations';
      });
    // })

    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  showModal(pkg)
  {
    console.log("Click here")
    this.modalDetail.show(pkg)
  }

  showStatutPackage(infos:{pkg:Package,transaction:Transaction,provider:Provider})
  {
    return getUiIconFromStatusPackage(infos.pkg,infos.transaction)
  }
  acceptPrice() {
    // this.notification.showNotification('top','center', 'danger', 'pe-7s-close-circle', '\<b>Sorry\</b>\<br>This service was not available now. Tray later.')

  }
  showMessage()
  {
    // this.header.s
  }

}
