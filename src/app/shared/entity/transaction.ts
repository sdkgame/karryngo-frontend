import { Entity } from "./entity";
import { Package } from "./package";

export enum TransactionState
{
    INIT,
    SERVICE_ACCEPTED_AND_WAITING_PAIEMENT,
    SERVICE_PAIEMENT_DONE_AND_WAITING_START,
    SERVICE_RUNNING,
    SERVICE_DONE_AND_WAIT_PROVIDER_PAIEMENT,
    SERVICE_PROVIDER_PAIEMENT_DONE,
    SERVICE_END,
}

export class Transaction extends Entity
{
    state:TransactionState=TransactionState.INIT;
    idProvider:String="";
    idRequester:String="";
    price:String="";
}
