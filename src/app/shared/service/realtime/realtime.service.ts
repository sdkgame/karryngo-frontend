import { Injectable } from '@angular/core';
import { Provider } from '../../entity/provider';
import { ApiService } from '../api/api.service';
import { AuthService } from '../auth/auth.service';
import { EventService } from '../event/event.service';
import { RealTimeChatMessageType, RealTimeErrorType, RealTimeInitErrorType, RealTimeInitMessageType, RealTimeMessage, RealTimeMessageType, UNKNOW_RECEIVER } from './realtime-protocole';

declare var io:any



@Injectable()
export class RealtimeService {
  
  currentUser:Provider;
  private socketClient:any=
  io("http://127.0.0.1:8090",
    {
      autoConnect:false,
      forceNew:true
    }
  );
  constructor(private eventService:EventService,    
    private authService:AuthService,    
    private apiService:ApiService) {

    //Event subscription
    this.authService.currentUserSubject.subscribe((userData:Provider)=>{
      this.currentUser=userData;
    })

    this.eventService.logoutEvent.subscribe((value:boolean)=>{
      // if(!value) return;
      //   this.socketClient.emit(RealTimeInitMessageType.LOGOUT,{
      //     senderID:this.currentUser.id,
      //     receiverID:UNKNOW_RECEIVER,
      //     type:RealTimeInitMessageType.LOGOUT
      //   })
      //   this.socketClient.disconnect(true);  
    })
    
    // let sub=this.eventService.loginEvent.subscribe((value)=>{  
    //   if(!value) return ;
      this.socketClient.connect()

      // //Socket.IO event handler     
      this.socketClient.on(RealTimeInitMessageType.NEW_CONNECTION, () => this.handleRealTimeSocketConnected());
      this.socketClient.on(RealTimeInitErrorType.CONNEXION_ERROR, (e) => this.handleRealTimeSocketErrorConnexion(e) );
      this.socketClient.on(RealTimeInitErrorType.CONNEXION_TIME_OUT, (e) => this.handleRealTimeSocketTimeOutErrorConnexion(e));
      this.socketClient.once(RealTimeInitMessageType.LOGGIN,(data:RealTimeMessage)=>this.handleRealtimeLoggin(data))       
      this.socketClient.on(RealTimeInitMessageType.DISCONNECT,()=>this.handleRealTimeSocketDisconnect())
    // })
    // sub.unsubscribe();
   }
   handleRealTimeSocketDisconnect()
   {
    this.socketClient.removeAllListeners()
   }
   handleRealTimeSocketTimeOutErrorConnexion(e:any)
   {
    console.error("connection timeout error: ", e)
   }
   handleRealTimeSocketErrorConnexion(e:any)
   {
    console.error("connect error: ", e)
   }

   handleRealTimeSocketConnected(){
    this.socketClient.emit(RealTimeInitMessageType.LOGGIN,
      {
        senderID:this.currentUser.id,
        receiverID:UNKNOW_RECEIVER,
        type:RealTimeInitMessageType.LOGGIN,
        data:{
          token:this.apiService.getAccessToken()
        }
      })
      console.log("Socket.IO Connected")
   }
   handleRealtimeLoggin (data:RealTimeMessage){
    if(data.error==RealTimeInitErrorType.SUCCESS )
    {
      this.eventService.loginRealtimeEvent.next(true);
      // this.eventService.loginRealtimeEvent
      // this.socketClient.off(handleRealtimeLoggin)
    }
  }
   addListener(event:RealTimeMessageType,callback:(data:RealTimeMessage)=>void):void
   {
     this.socketClient.on(event.toString(),(data:RealTimeMessage)=>callback(data));
   }
   send(data:RealTimeMessage)
   {
    this.socketClient.emit(data.type.toString(),data);
   }
   removeListener(event:RealTimeMessageType,callback:(data:RealTimeMessage)=>void):void
   {
      this.socketClient.off(event,callback);
   }
   addOnceListener(event: RealTimeMessageType, callback: (data: RealTimeMessage) => void) {
    this.socketClient.once(event.toString(),(data:RealTimeMessage)=>callback(data));
  }
 
}
