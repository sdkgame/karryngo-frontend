import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { ToastrService } from 'ngx-toastr';
import { Package, packageBuilder, PackageState } from '../../../shared/entity/package';
import { ProviderService } from './provider.service';
import { Provider,ServiceOfProvider, Zone } from '../../entity/provider';
import { EventService } from '../event/event.service';
import { BehaviorSubject } from 'rxjs';
import { TransactionService } from './transaction.service';
import { Transaction, TransactionState } from '../../entity/transaction';
import { RealTimeMessage } from '../realtime/realtime-protocole';



@Injectable({
    providedIn: 'root'
})
export class PackageService {
    packages: Map<String,Package>=new Map<String,Package>();
    packageList:BehaviorSubject<Map<String,Package>> = new BehaviorSubject<Map<String,Package>>(this.packages)
    headers = {};
    constructor(
        private api: ApiService,
        private toastr: ToastrService,
        private transactionService:TransactionService,
        private providerService:ProviderService,
        private eventService:EventService
    ) { 
        this.eventService.loginEvent.subscribe(()=>{
            this.headers = {
                'Authorization': 'Bearer ' + this.api.getAccessToken(),
                'Content-Type': 'application/json',
                // 'Accept': 'application/json'
              };
              this.getAllPackagesUser();
        });

        this.eventService.findPackageEvent.subscribe((packageId:string)=>{
            this.findPackageById(packageId);
        })
    }


    handlePackageModificationMessage(data:RealTimeMessage)
    {
        // if(!data.data || !data.data.type)
    }


    findLocalPackagesById(id: String): Package {
        if (this.packages.has(id)) { return this.packages.get(id); }
        return null;
    }
    findPackageById(id: String): Promise<Package> {
        if(id.length==0) return;
        return new Promise((resolve, reject) => {
            if (this.packages.has(id.toString()))  resolve(this.packages.get(id.toString()));
            else {
                this.api.get(`requester/service/${id}`,this.headers).subscribe(success=>{
                      if(success && success.resultCode==0)
                      {
                          let pack:Package =packageBuilder(success.result);
                          if(!this.packages.has(pack.id.toString()))
                          {
                            this.packages.set(pack.id.toString(),pack);
                            this.packageList.next(this.packages)
                          }
                         resolve(pack);
                      }
                      else reject(null);
                  }, (error: any)=> reject(null))
            }
        })
    }

   
    getPackageList()
    {
        let list:Package[]=[];
        for(const key in this.packages)
        {
            list.push(this.packages.get(key));
        }
        return list;
    }

    getAllPackagesUser(): Promise<any> {
        return new Promise((resolve, reject) => {
          this.api.get('requester/service/list', this.headers)
          .subscribe((response: any) => {
            if (response) {
                if(response.resultCode==0)
                {
                    response.result.map((pkg:Record<string,any>)=>{
                        let pack:Package= packageBuilder(pkg);
                        if(!this.packages.has(pack.id)) this.packages.set(pack.id.toString(),pack);
                        this.transactionService.setTransactionFromAPI(pkg);
                    })
                    this.packageList.next(this.packages);
                    resolve(response);
                }
                reject(response)        
            }

          }, (error: any) => {
              reject(error);
          });
        });
      }


// permet d'enregistrer un package
    packageCreation(data: Package): Promise<any> {
        return new Promise((resolve, reject) => {
            
            // console.log("Header ",headers);

            this.api.post('requester/service/add', data.toString(), this.headers)
            .subscribe(success => {
                if(success.resultCode === 0)
                {                
                    // this.packages.set(success.result.idService,data);
                    //this.toastr.success('You have been successfully Register your package!');               
                    resolve(success.result.idService);
                }
                else
                {
                    reject(success);
                }
            }, error => {
                //this.toastr.success(error.message);
                reject(error);
            });
        })

    }
    // permet d'update les infos d'un package
    updatePackage(nid: string, data: Package): Promise<any> {

        return new Promise((resolve, reject) => {

            this.api.post(`requester/service/${nid}`, data.toString(), this.headers)
            .subscribe(success => {
                if(success && success.resultCode==0)
                {
                    this.packages.set(success.result.idService,data);
                    
                    //this.toastr.success('You have been successfully Register your package!');
                    resolve(success);
                }
                else reject(success);
            }, error => {
                reject(error);
            });
        });
    }

    acceptPackagePrice(idService:String , transaction: Transaction):Promise<any> {
        return new Promise<any>((resolve,reject)=>{
            this.api.post('requester/service/transaction/accept_price',
            {  idService,idTransaction:transaction.id},
                this.headers
            ).subscribe((result)=>{
                console.log(result);
                if(result && result.resultCode==0)
                {
                    let pck:Package= this.packageList.getValue().get(idService);
                    pck.idSelectedTransaction=transaction.id.toString();
                    pck.idSelectedProvider=transaction.idProvider.toString(),
                    pck.state=PackageState.SERVICE_IN_TRANSACTION_STATE;
                    this.transactionService.transactionList.getValue().get(transaction.id).price=pck.suggestedPrice;
                    this.transactionService.transactionList.getValue().get(transaction.id).state=TransactionState.SERVICE_ACCEPTED_AND_WAITING_PAIEMENT;
                    resolve(true);
                }
                reject(result);
            },(error)=>{
                console.log(error)
                reject(error)
            });
        })
    }

    updatePrice(idPackage:string,idTransaction:String,price:string)
    {
        return new Promise<any>((resolve,reject)=>{
            this.api.post("requester/service/transaction/update_price",
            {
                price,
                idTransaction
            },this.headers).subscribe((result)=>{
                if(result && result.resultCode==0) 
                {
                    this.packageList.getValue().get(idPackage).suggestedPrice=price;
                    this.transactionService.transactionList.getValue().get(idTransaction).price=price;
                   return resolve(true);
                }
                reject(result);
            },(error)=>reject(error));
        })
    }
    confirmRequesterPaymentToPlatform(transaction:Transaction):Promise<any>
    {
        return new Promise<any>((resolve,reject)=>{
            this.api.post("requester/service/make_paiement",
            { idTransaction:transaction.id },this.headers).subscribe(
                (result)=>{
                    if(result && result.resultCode==0)
                    {
                        this.transactionService.transactionList.getValue().get(transaction.id).state=TransactionState.SERVICE_PAIEMENT_DONE_AND_WAITING_START;
                        return resolve(true);
                    }
                    reject(result);
                },
                (error)=>reject(error)
            )
        });
    }
    startPackageTransport(transaction:Transaction):Promise<any>
    {
        return new Promise<any>((resolve,reject)=>{
            this.api.post("requester/service/start",{idTransaction:transaction.id},this.headers).subscribe(
                (result)=>{
                    if(result && result.resultCode==0)
                    {
                        this.transactionService.transactionList.getValue().get(transaction.id).state=TransactionState.SERVICE_RUNNING;
                        return resolve(true);
                    }
                    reject(result)
                },
                (error)=>reject(error)
            )
        })
    }

    endPackageTransport(transaction:Transaction):Promise<any>
    {
        return new Promise<any>((resolve,reject)=>{
            this.api.post("requester/service/end",{idTransaction:transaction.id},this.headers).subscribe(
                (result)=>{
                    if(result && result.resultCode==0)
                    {
                        this.transactionService.transactionList.getValue().get(transaction.id).state=TransactionState.SERVICE_DONE_AND_WAIT_PROVIDER_PAIEMENT;
                        return resolve(true);
                    }
                    reject(result)
                },
                (error)=>reject(error)
            )
        })
    }

    

}
