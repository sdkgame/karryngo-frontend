import { Injectable } from '@angular/core';
import { BehaviorSubject, from, Observable, Subject } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { Discussion, Message } from '../../entity/chat';
import { ApiService } from '../api/api.service';
import { EventService } from '../event/event.service';
import { RealtimeService } from '../realtime/realtime.service';
import { TransactionService } from './transaction.service';

@Injectable({
    providedIn: 'root'
})
export class ChatService {
    listDiscusion: Discussion[] = [];
    listUnreadMessage: Message[] = [];
    listDiscusionSubject: BehaviorSubject<Discussion[]> = new BehaviorSubject<Discussion[]>(this.listDiscusion);
    listMessageUnreadSubject: BehaviorSubject<Message[]> = new BehaviorSubject<Message[]>([]);
    headers = {};

    constructor(private api: ApiService,
        private eventService:EventService,
        private transactionService:TransactionService,
        private realtimeService:RealtimeService) {
        this.headers = {
            'Authorization': 'Bearer ' + this.api.getAccessToken(),
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };
        this.eventService.loginEvent.subscribe((state)=>{
            // if(!state) return;
            this.listDiscusion=[];
            this.getDiscutionList()
            .then((result) => {
                
                this.emitDiscussion();
                // this.emitUnReadMessage();
            });
        })
    }

    getDiscutionList(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.api.get('chat/list', this.headers)
                .subscribe((success) => {
                    if (success && success.resultCode == 0) {
                        success.result.forEach((disc) => this.listDiscusion.push(Discussion.hydrate(disc)));
                        resolve(true);
                    }
                    else  reject(success);
                }, (error: any) => reject(error));
        });
    }

    getLocalDiscutionById(idDiscussion: String): Discussion {
        return this.listDiscusion.find((discuss: Discussion) => discuss._id == idDiscussion);
    }
    getUnReadDiscussion(): Observable<Discussion>{
        return this.listDiscusionSubject.pipe(
            switchMap((disc:Discussion[])=>from(disc)),
            filter((disc:Discussion)=>disc.read==0),
            map((disc:Discussion)=>{
              // this.eventService.findTransactionEvent.next({idProjet:disc.idProject.toString(),idTransaction:disc.idTransaction.toString()})
              this.eventService.findPackageEvent.next(disc.idProject.toString())   
              this.transactionService.getTransactionById(disc.idTransaction)     
              return disc;
            })
        )
    }
    getUnreadMessage() {
        this.listDiscusion.forEach((value: Discussion) => {
            this.listUnreadMessage.concat(value.chats.filter((msg: Message) => msg.read == 0));
        });
        this.emitUnReadMessage();
        // this.listDiscusion.filter((value:Discussion,)=>)
    }
    emitDiscussion() {
        this.listDiscusionSubject.next(this.listDiscusion.slice());
    }
    emitUnReadMessage() {
        this.listMessageUnreadSubject.next(this.listUnreadMessage.slice());
    }
    markAsRead(idMessage: String, idDiscussion: String): Promise<any> {
        return new Promise((resolve, reject) => {
            this.api.post('chat/mark_as_read', { idMessage, idDiscussion }, this.headers)
                .subscribe((success) => {
                    if (success && success.resultCode == 0) { resolve(success); }
                    else { reject(success); }
                }, (error: any) => reject(error));
        });
    }

    newMessage(msg: Message, discussID: String): Promise<any> {
        return new Promise((resolve, reject) => {
            this.api.post('chat/message/add', msg.toString(), this.headers)
                .subscribe((success) => {
                    if (success && success.resultCode == 0) { resolve(success); }
                    else { reject(success); }
                }, (error: any) => reject(error));
        });
    }
}
