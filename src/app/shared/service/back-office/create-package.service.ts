import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ColisPackage, Package } from '../../entity/package';
import { ServiceOfProvider, Zone } from '../../entity/provider';
import { PackageService } from './package.service';
import { ProviderService } from './provider.service';

export enum PackageCreationState
{
  SUBMIT_FORM,
  PROVIDER_FOUND,
  SUBMIT_PACKAGE_CREATION
}

@Injectable({
  providedIn: 'root'
})
export class CreateColisPackageService
{
  state:BehaviorSubject<PackageCreationState>=new BehaviorSubject<PackageCreationState>(PackageCreationState.SUBMIT_FORM);  
  foundProviders:ServiceOfProvider[]=[];
  package:ColisPackage=new ColisPackage();
  constructor(protected packageService:PackageService,protected providerService:ProviderService) { }

  
  findProvider():Promise<any> {
    return new Promise<any>((resolve,reject)=>{
      this.providerService.findProvider(this.package.from,this.package.to)
      .then((result:ServiceOfProvider[])=>{
        this.foundProviders= result;
        this.state.next(PackageCreationState.PROVIDER_FOUND);
        resolve(null);
      })
      .catch((error)=> {
        this.state.next(PackageCreationState.PROVIDER_FOUND);
        reject(error)
      })
    })
  }

  save():Promise<any>
  {
    return new Promise<any>((resolve,reject)=>{
      this.packageService.packageCreation(this.package)
      .then((id)=>{
        this.package.id=id;
        resolve(null);
      })
    })
  }
  
}
